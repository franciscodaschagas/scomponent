export const isComponent = (vnode, name) => {
  return vnode.componentOptions.tag === name
}

export const getComponents = (vnode, name) => {
  return vnode.$children && vnode.$children.length
    ? vnode.$children.filter(child => isComponent(child.$vnode, name))
    : []
}
