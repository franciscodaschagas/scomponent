# COMPONENTES

## Core

O componentente `Core` é um núcleo idealizado para envolver toda a aplicação como componente imediato após o elemento raiz (_e. g_ `div#app`). Esse componente fornece uma customização global e um _provider_ que torna acessível metódos de componentes internos para toda a aplicação

#### Template

```html
<Core>
  <!-- Outros componentes da aplicação -->
</Core>
```

#### Propriedades

| Nome           | Tipo   | Requrido | Padrão      | Descrição                                                                                |
| -------------- | ------ | -------- | ----------- | ---------------------------------------------------------------------------------------- |
| `main-color`   | String | Não      | `'#4795FD'` | Define a cor principal para todos os elementos internos que acessam a interface de Core  |
| `second-color` | String | Não      | `'#343a40'` | Define a cor secundária para todos os elementos internos que acessam a interface de Core |

### Métodos

Os métodos listados são acessados via a API _inject_ do Vue

| Nome        | Parâmetros          | Retorno | Descrição                                                                       |
| ----------- | ------------------- | ------- | ------------------------------------------------------------------------------- |
| provider    | -                   | Object  | Disponibiliza métodos para todos o componentes internos a Core                  |
| addProvider | provider (Function) | -       | Adiciona um método a interface do provider para ser visível em toda a aplicação |

## Navbar

O componente `Navbar` é o padrão para a barra de navegação superior que emgloba o menu principal e diversos menus secundários

#### Template

```html
<Navbar>
  <!-- Componentes a serem exibidos dentro da Navbar -->
</Navbar>
```

#### Propriedades

| Nome    | Tipo  | Requerida | Padrão | Descrição                                                                 |
| ------- | ----- | --------- | ------ | ------------------------------------------------------------------------- |
| `items` | Array | Sim       | Não    | Objeto que recebe todas as configurações relacionadas aos menus da Navbar |

#### `items` interface

```js
[
    {
        groupLabel: 'ButtonHeaderName' // Nome do grupo
        arrayButtons: [ // Botões daquele grupo
            {
                label: 'Pessoa', // Nome do botão
                tab: 'Person' // Referência de sua tela
            },
            {
                label: 'Funcionário',
                tab: 'Employee'
            }
        ],
        icon: 'Icon' // Ícone de cabeçalho do grupo
    }
    ...
]
```

## MenuUser

O componente `MenuUser` define o estilo e comportamento padrão a ser adotado nos sitemas

#### Template

```html
<MenuUser slot="area-nav" />
```

#### Propriedades

| Nome    | Tipo   | Requerida | Padrão | Descrição                                      |
| ------- | ------ | --------- | ------ | ---------------------------------------------- |
| `items` | Array  | Sim       | Não    | Lista de items a ser exibidos no corpo do menu |
| `user`  | Object | Sim       | Não    | Configurações básica de usuário                |

#### `items` interface

```js
[
    {
        label: 'Item 01', // Nome do item do menu
        icon: 'Icon01', // Ícone do item do menu
        handler: () => {} // Callback que excuta o click
    },
    {
        label: 'Item 02',
        icon: 'Icon02',
        handler: () => {},
        subitems: [
            // Array de objetos com a mesma interface de 'items' exceto 'subitems'
        ]
    },
    ...
]
```

#### `user` interface

```js
{
    name: 'John Doe', // Nome do usuário presente no menu
    handler: () => {} // Callback que excuta visualização de perfil
}
```

## Notifications

O componente `Notifications` é responsável por exibir e gerenciar uma lista de notificações daquele usuário

#### Template

```html
<Notifications slot="area-nav" />
```

#### Propriedades

| Nome     | Tipo     | Requerida | Padrão | Descrição                                            |
| -------- | -------- | --------- | ------ | ---------------------------------------------------- |
| `items`  | Array    | Não       | `[]`   | Uma lista de notificações                            |
| `remove` | Function | Sim       | Não    | Callback excutada na ação remoção de uma notificação |

#### `items` interface

```js
[
    {
        id: 1, // Identificação da notificação
        title: 'Título', // Título da noficação
        preview: 'Descrição da notificação', // Trecho da notificação
        read: false // Marcador de leitura
    }
    ...
]
```

#### `remove` interface

```js
// Espera por o id da notificação
const remove = id => {
  // TODO
}
```

## Port

O componente `Port` é responsável por renderizar as guias e telas do sistema

#### Template

```html
<Port />
```

#### Propriedades

| Nome      | Tipo    | Requerido | Padrão | Descrição                                |
| --------- | ------- | --------- | ------ | ---------------------------------------- |
| `screens` | Obeject | Sim       | Não    | Sequência de páginas a serem exibidas    |
| `initial` | Array   | Não       | Não    | Configura a página inicial a ser exibida |

#### `screens` interface

```js
import Tela01 from '@/views/TelaO1.vue'
import Tela02 from '@/views/TelaO2.vue'

const screens = {
  Tela01,
  Tela02
}
```

#### `initial` interface

```js
const initial = [
  'Tela 01', // Nome da tela
  'Tela01', // Nome da referência da tela,
  false // (true ou false) se a tela pode fechar ou não
]
```

## List

O componente `List` renderiza um conjunto de outros componentes formados por uma caixa de busca, botões de ações, tabela e componentes de paginação.

#### Template

```html
<List />
```

#### Propriedades

| Nome       | Tipo   | Requerido | Padrão | Descrição                        |
| ---------- | ------ | --------- | ------ | -------------------------------- |
| listConfig | Object | Não       | `{}`   | Objeto de configuração da lista. |

#### `listConfig` interface

```js
{
    loading: false, // Indica se a lista está carregando (spinner na tela). true para carregando e false para não carregando
    items: [{}, ...], // conteúdo da lista. Pode ser qualquer coisa
    onSelect: item => { // Função que callback que será chamada apos um clique na tupla da tabela
        // TODO
    },
    onDouble: item => { // Função que callback que será chamada apos um duplo clique na tupla da tabela
        // TODO
    },
    options: [
      // Um array de objetos que define a sequência de
      // botões de opções a serem mostrados ao lado da
      // na lista
      ...
      {
        icon: 'pen', // ícone do botão (Unicon)
        handler: () => {/* TODO */} // Callback executada no click
      }
      ...
    ]
}
```

## Screen

O componente `Screen` é responsável pela abstração de uma tela dos sistema. Em suma, toda tela que se queira desenvolver deve conter o `Screen` como componente _root_.

#### Template

```html
<Screen>
  <!-- TODO -->
</Screen>
```

**Antes de partir para a explicação do componente em si, é necessário destacar que por se tratar do componente mais complexo da aplicação e pelo uso constante por parte do programador, algumas peculiaridades devem ser apresentadas com maior rigor.**

Primeiramente é necessário entender que o `Screen` é composto por áreas demarcadas com `slot`. Sendo elas uma área de "prepend", uma área default e uma área de "append".

### Área de `prepend`

```html
<Screen>
  <div slot="prepend">
    <!-- TODO -->
  </div>
</Screen>
```

Tudo que estiver marcado com o `slot="prepend"` será renderizado imediatamente antes do conteúdo principal da `Screen`.

### Área de `append`

```html
<Screen>
  <div slot="append">
    <!-- TODO -->
  </div>
</Screen>
```

Tudo que estiver marcado com o `slot="append"` será renderizado imediatamente após o conteúdo principal `Screen`.

**Os `slots` `prepend` e `append` serão renderizados na seção de _action_ da `Screen`.**

#### Propriedades

| Nome         | Tipo    | Requerido | Padrão  | Descrição                                                                                                                                                                       |
| ------------ | ------- | --------- | ------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `def`        | Boolean | Não       | `true`  | Define se Screen é segue ou não o template padrão.                                                                                                                              |
| `menu`       | Object  | Não       | `{}`    | Objeto com os blocos que definem a configuração do `Sidebar`.                                                                                                                   |
| `listConfig` | Object  | Não       | `{}`    | Configuração da listagem padrão de `Screen`. Ver seção [list](#list).                                                                                                           |
| `operations` | Object  | Não       | `{}`    | _Callbacks_ a serem executadas na criação e atualização da entidade. A propridade `only` (`'save'`, `'list'` ou `'edit'`) define qual será o único botão de operação a aparecer |
| `enableOps`  | Object  | Não       | `{}`    | Objeto que habilita ou desabilita as operations (_create_, _update_, _list_, _cancel_). Por padrão, todas são habilitadas                                                       |
| `messages`   | Object  | Não       | `{...}` | Objeto contendo mensagens exibidas nos `Alert` padrão nas ações da propriedade `operations`.                                                                                    |

### Diretivas

| Nome      | Descrição                                                     |
| --------- | ------------------------------------------------------------- |
| `v-model` | Atribuito reativo do tipo objeto que representa uma entidade. |

### Eventos

| Nome           | Argumentos                                                                                                                                                                                                            | Descrição                                                                                                                                                                              |
| -------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `form`         | <`"see"` \| `"edit"` \| `"keep"`>                                                                                                                                                                                     | Evento responsável por levar a `Screen` à área de formulário. Caso não seja informado nenhum argumento, o evento padrão é disparado (mudança da área de _actions_ para área de _form_) |
| `action`       | Não                                                                                                                                                                                                                   | Evento responsável por levar a `Screen` à área de ação.                                                                                                                                |
| `alert`        | (`"success"` \| `"error"`, [`"save"` \| `"remove"`])                                                                                                                                                                  | Evento responsável por chamar um `Alert` com as configuraçoes passadas.                                                                                                                |
| `disabled`     | `true` \| `false`                                                                                                                                                                                                     | Evento que habilita ou desabilita os campos da área de formulário.                                                                                                                     |
| `createscreen` | (`{...}`, [`{...}`, `[...]`])                                                                                                                                                                                         | Evento que cria uma nova página a partir das configurações contidas no seus parâmetros. [descrição de `createscreen`](#createscreen-interface)                                         |
| `message`      | [`"info"`\|`"warn"`\|`"success"`\|`"error"`, `"mensagem aqui..."`, `3.5`]. Primeiro é o tipo, segundo a mensagem exibida e por último a duração. Caso a duração não seja passada, o usuário tem que fechar a mensagem | Criar um novo alerta de mensagem e coloca no topo de uma pilha de no máximo 9 mensagens dentro do Core                                                                                 |

Segue um exemplo de disparo de evento:

```js
this.$emit('form', 'keep')
```

O exemplo acima faz a página da tela de ação para a tela de formulário. O argumento `"keep"` matém o estado da entidade durate a transição

#### `createscreen` interface

```js
this.$emit(
  'createscreen',
  /* Configuração básica da página (obrigatório) */

  {
    label: 'Tela 01', // nome que aparece na aba
    tab: 'Tela01' // nome da referência da tela
  },

  /* Objeto nativo da options da API de renderização do Vue (Opcional)*/
  {
    // TODO
  },

  /* Array de VNodes filhos da página (Opcional) */
  [
    // TODO
  ]
)
```

### Detalhando a `Screen`

Por padrão o conteúdo da área de ação da `Screen` consiste em uma listagem. Qualquer conteúdo passado dentro da `Screen` que não esteja contido nos `slots` `append` ou `prepend` irá substituir o conteúdo padrão.

A área de formulário da `Screen` requer um `slot` especial envolto pela _tag_ nativa `<template>`. Segue um exeplo da marcação:

**Exemplo**

```html
1|
<Screen>
  2|
  <template v-slot:form="f">
    3| <component :is="f.current" :disabled="f.disabled" :entity="test" /> 4|
  </template>
  5|
</Screen>
```

É necessário que os componentes de formulário estejam dentro do `<template>`, pois as propriedades internas de controle de formulário de `Screen` só podem ser acessadas via captura do objeto pela diretiva `v-slot` chamada com o parâmetro `form` (linha 2). O objeto que contem as propriedades pode receber qualquer nome, toda via, é recomendado algo sugestivo e simples como a letra "f"(que significa formulário).

**A diretiva `v-slot:form="f"` só pode conter esta assinatura caso faça parte de `<template>.`**

#### Objeto de controle

O objeto de controle é o elemento capturado pela diretiva `v-slot:form` (na linha 2 representado por "f"), com as seguintes propriedades:

| Nome       | Tipo    | Requerida | Descrição                                                                 |
| ---------- | ------- | --------- | ------------------------------------------------------------------------- |
| `current`  | String  | Não       | Aponta para o formulário atual em exibição.                               |
| `disabled` | Boolean | Sim       | Define se os campos dos formulários estarão habilitados ou desabilitados. |

## FormBasic

O componente `Form` é responsável por envolver e dar a estrutura básica para comportar elementos de formulários (_e. g_ campos de texto).

#### Template

```html
<FormBasic>
  <!-- TODO -->
</FormBasic>
```

## FormLabel

O componente `FormLabel` é responsável por definir um label entre os _inputs_ de formulários. Ele comprende toda a largura do elemento pai (no caso mais ideal, o `FormBasic`).

#### Template

```html
<form-label>Seu texto aqui</form-basic>
```

> Obs.: A cor assumida pelo `FormLabel` deve ser a cor defina na propriedade `main-color` do `Core`.

## Inputs especiais

Está seção está divida de acordo com os _inputs_ presentes no escopo da SComponents

### _Search_

O componente `InputSearch` define um espaço para a inserção de qualquer _input_ dentro. Ao lado, é renderizado um botão padrão que toma uma ação via propriedade

#### Template

```html
<input-search>
  <!-- O input desejado vem aqui -->
</input-search>
```

#### Propriedades

| Nome     | Tipo     | Requerido | Padrão  | Descrição                             |
| -------- | -------- | --------- | ------- | ------------------------------------- |
| handler  | Function | Sim       | -       | Callback executada no click.          |
| disabled | Boolean  | Não       | `false` | Habilita ou desabilita o botão padrão |

### _Date_

O componente `InputDate` renderiza um _input_ de texto comum, porém é atrelado a ele um calendário dinâmico que é acionado ao evento de foco no _input_. O calendário é um adpatação para Vue da biblioteca [flatpickr](https://flatpickr.js.org/).

#### Template

```html
<input-date v-model="..." />
```

#### Propriedades

| Nome    | Tipo   | Requerido | Padrão | Descrição                                                                                                |
| ------- | ------ | --------- | ------ | -------------------------------------------------------------------------------------------------------- |
| options | Object | Não       | `{}`   | Objeto de configuração do calendário. Para ver as opções entre [aqui](https://flatpickr.js.org/options/) |

> Obs.: Como é uma abstração simples da flatpickr, então algumas das opções estão travadas. Por essa mesma razão, não é garantido que todas as opções oferecidas vão funcionar como o esperado.

### _Money_

O componente `InputMoney` é uma abstração do input [v-money](https://github.com/vuejs-tips/v-money) em um _input_ para a mostragem de um valor numérico na forma de moeda.

#### Template

```html
<input-money v-model="..." />
```

#### Propriedades

| Nome    | Tipo   | Requerido | Padrão | Descrição                                                                                                            |
| ------- | ------ | --------- | ------ | -------------------------------------------------------------------------------------------------------------------- |
| options | Object | Não       | `{}`   | Objeto de configuração da máscara. Para ver as opções entre [aqui](https://github.com/vuejs-tips/v-money#properties) |

> Obs. 1: Como é uma abstração simples da v-money não é garantido que todas as opções oferecidas vão funcionar como o esperado.

> Obs. 2: A máscara padrão é defina para a moeda brasileira. Então o prefixo é `R$` a pontuação `,` (decimais) e `.` (milhares).

## Icon

O componente `Icon` é responsável por gerar um ícone (do pacote [Unicon](https://antonreshetov.github.io/vue-unicons/)) de forma dinâmica.

#### Template

```html
<Icon />
```

#### Propriedades

| Nome         | Tipo   | Requerido | Padrão           | Descrição                                |
| ------------ | ------ | --------- | ---------------- | ---------------------------------------- |
| `name`       | String | Sim       | Não              | O nome do ícone a ser renderizado        |
| `fill`       | String | Não       | `"currentColor"` | Define a cor do ícone                    |
| `width`      | String | Não       | `"24px"`         | Define a largura do ícone                |
| `height`     | String | Não       | `"24px"`         | Define a altura do ícone                 |
| `icon-style` | String | Não       | `"line"`         | Define se o ícone é preenchido ou vazado |
