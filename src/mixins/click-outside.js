import ClickOutside from 'vue-click-outside'

export default {
  methods: {
    hide() {
      this.drop = false
    }
  },

  directives: {
    ClickOutside
  }
}
