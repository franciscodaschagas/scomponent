export default {
  name: 'Screen',

  data() {
    return {
      show: true, // Se estar ou não no DOM Real
      parentAttrs: {} // Atributos do componente Parent,
    }
  },

  methods: {
    // Remover elemento do DOM Real
    async disappear() {
      this.show = false
    },

    // Remove elemento do DOM Virtual
    async close(key) {
      if (key === this.parentAttrs.rg) {
        await this.disappear()
        this.$parent.$destroy()
      }
    },

    // Configura a página de formulário a ser exibida primeiro
    setCurrent() {
      let active = false

      if (this.menu.forms && this.menu.forms.length) {
        for (const form of this.menu.forms) {
          if (form.active) {
            this.current = form.component
            active = form.active
            break
          }
        }

        if (!active) {
          this.current = this.menu.forms[0].component
        }
      }
    },

    // Externa o valor de this.disabled para os contexto de formulário
    setDisabled(value) {
      this.disabled = value
    },

    // Define o método que muda os estados da própria Sidebar
    setStateSidebar(callback) {
      this.$set(this, 'stateSidebar', callback)
    },

    // Configura a entidade (v-model) para o seu estado original
    entityClear() {
      this.setCurrent()
      this.$emit('input', JSON.parse(this.entityCopy))
    },

    // Volta para o contexto de formulário
    toForm(value) {
      if (value === 'edit') {
        this.stateSidebar({
          defineOperation: true,
          defineButtons: true
        })
      } else if (value === 'see') {
        this.stateSidebar({
          defineOperation: true,
          defineButtons: false
        })
        this.disabled = true
      } else if (value === 'keep') {
        this.stateSidebar({
          defineOperation: false,
          defineButtons: true
        })
      } else {
        this.entityClear()
      }

      this.view = 'forms'
    },

    // Volta para o contexto de ação
    toAction(conf) {
      // Verifica se deve ser conferido se houve edição
      if (conf) {
        if (JSON.stringify(this.value) !== this.entityCopy) {
          this.modalCancel = true
          return
        }
      }

      this.stateSidebar({
        defineOperation: false,
        defineButtons: true
      })
      this.entityClear()
      this.disabled = false
      this.view = 'actions'
    },

    // Muda o valor e this.stateAction para coordenar os alertas
    toActionState(value) {
      this.actionState = value
    },

    // Aciona um Alert (alerta)
    toAlert(type, origin = 'save') {
      if (origin === 'save') {
        this.alert = true
        this.typeAlert = type
      } else if (origin === 'remove') {
        this.alertRemove = true
        this.typeAlertRemove = type
      }
    },

    toMessage(...params) {
      const [type, message, time] = params

      const msn = {
        id: 'm' + new Date().getTime(),
        type,
        message,
        time
      }

      this.$set(this, 'message', [...this.message, msn])
    },

    // Exibe o modal de confirmação
    toConfirm(...data) {
      this.confirmText = data[0]
      this.confirmHandler = data[1]
      this.modalConfirm = true
    },

    // Limpa o estado do que é passado como parâmetro
    toClear(type) {
      if (type === 'entity') {
        this.entityClear()
      }
    },

    // Muda a página de formulário mediante click no this.menu.forms
    changeForm(component) {
      this.current = component
    }
  },

  computed: {
    // Retorna um valor de verdade se a página atual foi aberta por outra
    cameFrom() {
      return (
        this.externalConfig.parentRg &&
        typeof this.externalConfig.parentRg === 'number'
      )
    },

    // Verifica se os elementos do menu de ações do Aside estão ou não ativos
    actionActivation() {
      return JSON.stringify(this.value) !== this.entityCopy
    }
  },

  inject: {
    provider: {
      type: Function,
      default: () => {}
    }
  }
}
