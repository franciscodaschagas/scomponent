import Vue from 'vue'
import VueTabsChrome from 'vue-tabs-chrome'
import { Plugin } from 'vue-fragment'
import Unicon from 'vue-unicons'
import Tulipa from 'tulipa-vue'
import 'flatpickr/dist/flatpickr.min.css'
import 'tulipa-vue/dist/tulipa.css'
import './style.css'

import Core from '@/components/Core.vue'
import Icon from '@/components/Icon.vue'
import Navbar from '@/components/Navbar.vue'
import MenuUser from '@/components/MenuUser.vue'
import Messages from '@/components/Messages.vue'
import Notification from '@/components/Notification.vue'
import Guide from '@/components/Guide.vue'
import Port from '@/components/Port.vue'
import Screen from '@/components/Screen.vue'
import List from '@/components/List.vue'
import Alert from '@/components/Alert.vue'
import ModalCancel from '@/components/ModalCancel.vue'
import ModalList from '@/components/ModalList.vue'
import FormBasic from '@/components/FormBasic.vue'
import FormLabel from '@/components/FormLabel.vue'
import InputMoney from '@/components/InputMoney.vue'
import InputSearch from '@/components/InputSearch.vue'
import InputDate from '@/components/InputDate.vue'
import Avatar from '@/components/Avatar.vue'
import Money from 'v-money'

Vue.use(Tulipa)
Vue.use(VueTabsChrome)
Vue.use(Plugin)
Vue.use(Unicon)
Vue.use(Money)

Vue.config.productionTip = false

const COMPONENTS = {
  Core,
  Icon,
  Navbar,
  MenuUser,
  Messages,
  Notification,
  Guide,
  Port,
  Screen,
  List,
  Alert,
  ModalCancel,
  ModalList,
  FormBasic,
  FormLabel,
  InputMoney,
  InputSearch,
  InputDate,
  Avatar
}

Object.keys(COMPONENTS).forEach(key => {
  Vue.component(key, COMPONENTS[key])
})

// Isto permite com que os eventos se propaguem pela árvore de componentes
Vue.use(Vue => {
  Vue.prototype.$bubble = function(name, ...args) {
    let component = this
    do {
      component.$emit(name, ...args)
      component = component.$parent
    } while (component)
  }
})

export default {
  install(Vue) {
    for (const name in COMPONENTS) {
      Vue.component(name, COMPONENTS[name])
    }
  }
}
