export default [
  {
    groupLabel: 'Favoritos',
    arrayButtons: [],
    icon: 'bookmark'
  },
  {
    groupLabel: 'Cadastros funcionais',
    icon: 'bag',
    arrayButtons: [
      {
        label: 'Funcionario',
        tab: 'Facebook'
      },
      {
        label: 'Secretaria',
        tab: 'Facebook'
      },
      {
        label: 'Teste',
        tab: 'Test'
      },
      {
        label: 'Teste 2',
        tab: 'Test2'
      }
    ]
  }
]
